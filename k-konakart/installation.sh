#/bin/bash

if [ ! -f /usr/local/konakart ]; then
	if [ -z "$DB_TYPE"]; then
		DB_TYPE="mysql"
		echo 'db type set to mysql'
	fi
	if [ -z "$DB_ADDRESS" ]; then
		echo "No DB address is set. Exiting..."	
		exit 1	
	fi
	echo $DB_ADDRESS
	if [ -z "$DB_PORT" ]; then
		DB_PORT="3306"
		echo 'db port set to 3306'
	fi
	echo $DB_PORT
	if [ -z "$DB_ROOT_PWD" ]; then
		DB_PWD="dynatrace"
		echo 'db root pwd set'
	fi
	
	if [ -z "$DTAGENT_NAME" ]; then
		DTAGENT_NAME="konakart"
		echo 'dtagent name is set to konakart'
	else
		echo 'dtagent name is set to $DTAGENT_NAME'
	fi

	echo "Installing ......"
	/tmp/konakart-installation -S -DJavaJRE /usr/lib/jvm/java-1.7.0-openjdk-amd64 -DDatabaseType "$DB_TYPE" -DDatabaseUrl jdbc:"$DB_TYPE"://"$DB_ADDRESS":"$DB_PORT"/konakart?zeroDateTimeBehavior=convertToNull -DDatabaseUsername root -DDatabasePassword "$DB_PWD"
	sleep 10s	
	echo "...... Finished installing."

	echo "Installing dynatrace agent"
	cd /opt/ && echo -e Y\\nN\\n/opt/dynatrace/6.0.0\\nY | java -jar /opt/dynatrace-agent-6.2.0.1239-unix.jar

	if [ -z "$DTCOLLECTOR" ]; then
		echo 'dtcollector is not set. Skip instrumenting'
	else
		echo "Connecting agent..."
		touch /usr/local/konakart/bin/setenv.sh
		echo "export CATALINA_OPTS=\"-agentpath:/opt/dynatrace-6.2/agent/lib64/libdtagent.so=name=$DTAGENT_NAME,server=$DTCOLLECTOR\"" > /usr/local/konakart/bin/setenv.sh
	fi
fi
echo "Starting...."
#sed -i -e"s/0.0.0.0:/"

/bin/sh /usr/local/konakart/bin/startkonakart.sh
tail -f /usr/local/konakart/logs/catalina.out

#docker run
